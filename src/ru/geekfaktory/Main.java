package ru.geekfaktory;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
//123
        Scanner in = new Scanner(System.in);
        int a = 97;
        System.out.println("Задание 1  " + ((a % 10) + (a / 10)));
        //Задание 2
        int x = -5;
        double t;
        if (x > 0) {
            t = Math.pow(Math.sin(x), 2);
            System.out.println("Задание 2   у=Sin^2(x) y=" + t);
        } else {
            t = 1 - 2 * Math.sin(Math.pow(x, 2));
            System.out.println("Задание 2 у=1-2*sin(x^2) y=" + t);
        }
        //Задание 3
        int a1=12;
        int a2=14;
        int min = Math.min(a1, a2);
        int max = Math.max(a1, a2);
        System.out.println("Задание 3  " + " MAX=" + max + " MIN =" + min);
        //Задание 4
        int a3=3;
        int a4=2;
        double qa = Math.min(a3, a4 * 0.305);
        System.out.println("Задание 4  " + qa);
        //Задание 5 и 6
        int a5=10;
        int a6=5;
        double qw = Math.min(Math.pow((3.14 * a5), 2), Math.pow(a6, 2));
        System.out.println("Задание 5  " + qw);
        int z = a6 % a5;
        if (z == 0) {
            System.out.println("Задание 6  " + a6 / a5);
        } else {
            System.out.println("Задание 6  " + "брат, нацело не делится");
        }
        //Задание 7
        int u = 121;
        if (u / 100 == u % 10) {
            System.out.println("Задание 7 Число " + u + "  Полиндром");
        } else {
            System.out.println("Задание 7 Число " + u + "  НЕ Полиндром");
        }
        //Задание 8
        int u1=131;
        if (u1 / 100 == u1 % 10) {
            System.out.println("Задание 8  В числе есть одинаковые циифры");
        } else {
            if (u1 / 100 == u1 % 100 / 10) {
                System.out.println("Задание 8  В числе есть одинаковые циифры");
            } else {
                if (u1 % 100 / 10 == u1 % 10) {
                    System.out.println("Задание 8  В числе есть одинаковые циифры");
                } else {
                    System.out.println("Задание 8  В числе нет одинаковых циифр");
                }
            }
        }
        //Задание 9
        double s = -1;
        if (s <= 0) {
            System.out.println("Задание 9  y=" + 0);
        } else {
            if (0 < s & s <= 1) {
                System.out.println("Задание 9  y=" + s);
            } else {
                System.out.println("Задание 9  y=" + Math.pow(s, 2));
            }
        }
//Задание 10
        int ns = 7;
        for (int nm = 1; nm < 10; nm++) {
            System.out.println("Задание 10 " + nm + "*" + ns + "=" + nm * ns);
        }
//Задание 11
        System.out.println("Задание 11 Введите c клавиатуры значение для вывода таблицы умножения");
        int nu = in.nextInt();
        for (int nm = 1; nm < 10; nm++) {
            System.out.println("Задание 11 " + nm + "*" + nu + "=" + nm * nu);
        }
        //Задание 14
        System.out.println("Задание 14 Введите c клавиатуры значение для построения прямоугольного треугольника");
        int kl = in.nextInt();
        for (int ai = 1; ai <= kl; ai++) {
            for (int aj = 1; aj <= kl; aj++) {
                if (aj == ai) {
                    System.out.println("*");
                    break;
                } else {
                    System.out.print("*");
                }
            }
        }
//Задание 15
        System.out.println("Задание 15 Введите c клавиатуры значение для вывода пирамиды");
        int nh = in.nextInt();
        if (nh % 2 == 0) {
            int h = nh + 1;
            for (int ai = 1; ai <= h; ai++) {
                if (ai % 2 != 0) {
                    for (int aj = 0; aj < h; aj++) {
                        if ((aj < (h-ai)/2) | (aj > (h+ai)/2-1)) {
                            System.out.print(" ");
                        } else {
                            System.out.print("*");
                        }
                    }
                } else {
                    continue;
                }
                System.out.println();
            }
        } else {
            int h = nh;
            for (int ai = 1; ai <= h; ai++) {
                if (ai % 2 != 0) {
                    for (int aj = 0; aj < h; aj++) {
                        if ((aj < (h-ai)/2) | (aj > (h+ai)/2-1)) {
                            System.out.print(" ");
                            continue;
                        } else {
                            System.out.print("*");
                            continue;
                        }
                    }
                } else {
                    continue;
                }
                System.out.println();
            }
        }
        in.close();
        // write your code here
    }
}

